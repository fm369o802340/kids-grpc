mkdir xbuild
cd xbuild 
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_OSX_DEPLOYMENT_TARGET=10.11 ..
make -j 80
mkdir deploy
ag -g ".*\.a" | xargs -t -I % cp % deploy/
