mkdir xbuild
cd xbuild 
cmake -DCMAKE_BUILD_TYPE=Debug ..
make -j 5
mkdir deploy
ag -g ".*\.a" | xargs -t -I % cp % deploy/
